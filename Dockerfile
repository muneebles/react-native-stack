FROM openjdk:8-stretch

# Install NodeJS
RUN curl -sL https://deb.nodesource.com/setup_11.x | bash - \
		&& apt-get install -y nodejs

# sudo apt-get install gcc g++ make

# Install yarn
RUN curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && apt-get update \
    && apt-get install -y yarn

# Install react-native
RUN yarn global add react-native-cli

# Install watchman
RUN apt-get install -y \
	libtool
RUN cd /tmp \
	&& git clone https://github.com/facebook/watchman.git \
	&& cd watchman \
    && git checkout v4.9.0

RUN apt-get install -y libssl-dev autoconf automake pkg-config
RUN apt-get install -y g++ make
RUN apt-get install -y python-dev

RUN cd /tmp/watchman \
    && ./autogen.sh \
    && ./configure \
    && make \
    && make install \
    && rm -rf /tmp/watchman

# # Android sdk link
# # https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip
ENV ANDROID_SDK_VERSION 4333796
RUN mkdir -p /opt/android-sdk \
	&& cd /opt/android-sdk \ 
	&& ls -la \
	&& pwd \
	&& wget https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_VERSION}.zip \
	&& unzip *tools*linux*.zip \
	&& rm *tools*linux*.zip

ENV ANDROID_HOME /opt/android-sdk

# Accept the license agreements of the SDK components
COPY bin/license_accepter.sh /opt/
RUN /opt/license_accepter.sh $ANDROID_HOME

# sdkmanager "system-images;android-27;google_apis;x86"




