import React, { Component } from 'react'
import { AppRegistry } from 'react-native';
import { RootComponent } from './src/core/_root'
import { name as appName } from './app.json';

AppRegistry.registerComponent(appName, () => RootComponent );
