import { connect } from 'react-redux';
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, ScrollView } from 'react-native';
import { NavigationService } from './services/navigation'

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});


class HomeComponent extends Component {
  handlePress = () => {
    this.props.dispatch({ type: 'INCREMENT' })
  }

  handleRequest = () => {
    this.props.dispatch({
      type: "MY_OFFLINE_ACTION",
      meta: {
        offline: {
          effect: { url: `http://10.0.2.2:5000/test/${this.props.thing}` }
        }
      }
    })
  }

  handleNavigation = () => {
    NavigationService.navigate('Details', {
      text: 'This is the details view'
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Counter: {this.props.thing}</Text>
        <Text style={styles.welcome}>Hello, this is React Native!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
          <View style={styles.buttonLayout}>
            <View style={styles.button} >
              <Button
                onPress={this.handleNavigation}
                title="Navigate somewhere"/>
            </View>
          </View>
          <View style={styles.buttonLayout}>
            <View style={styles.button} >
              <Button
                onPress={this.handlePress}
                title="Increment counter"/>
            </View>
            <View style={styles.button} >
              <Button
                onPress={this.handleRequest}
                title="Perform request"/>
            </View>
        </View>
      </View>
    );
  }
}


function mapStateToProps(state) {
  return {
    thing: state.counter.counterValue
  }
}


export const Home = connect(mapStateToProps)(HomeComponent)


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 25,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  buttonScrollContainer: {
    overflow: 'scroll',
  },
  buttonLayout: {
    flexDirection: 'row'
  },
  button: {
    padding: 4,
    height: 45
  }
});
