import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, ScrollView } from 'react-native';

export class SimpleScreen extends Component {
  static defaultProps = {
    text: 'This is not some text'
  }

  render() {
    return (
      <View><Text>{this.props.text}</Text></View>
    )
  }
}
