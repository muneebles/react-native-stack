import { combineReducers } from 'redux'
import { createNavigationReducer } from 'react-navigation-redux-helpers';
import { reducer as formReducer } from 'redux-form'


import * as reducers from '../reducers'

// Integrate navigation into the redux store
import { TabNavigator } from '../navigator'
const navigationReducer = createNavigationReducer(TabNavigator);


export default combineReducers({
  navigation: navigationReducer,
  formReducer,
  ...reducers
})
