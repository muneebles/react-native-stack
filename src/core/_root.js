import React, { Component } from 'react'
import { BackHandler } from 'react-native';
import { Provider, connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import { reduxifyNavigator } from 'react-navigation-redux-helpers';

import { configureStore } from './_store'
import { TabNavigator } from '../navigator'
import { NavigationService } from '../services/navigation'


export class RootComponent extends Component {
	constructor(props) {
    super(props)
    this.state = {
      rehydrated: false
    }
	}

  onBackPress = () => {
    if (this.state.store.getState().navigation.index === 0) {
      return false;
    }

    this.state.store.dispatch(NavigationActions.back());
    return true;
  };

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
  }

  async componentWillMount() {
    // https://github.com/rt2zz/redux-persist/issues/126#issuecomment-291291806
    // Asynchronously configure store, waiting for persist rehydration
    const store = await configureStore()
    const rehydrated = true

    // Integrate navigation into the redux store:
    // Provide our external navigation service with a reference to dispatch actions
    NavigationService.setDispatchFunction(store.dispatch)

    // Wrap the navigator with a component that renders screens based on
    // navigation state in the redux store
    const App = reduxifyNavigator(TabNavigator, "root");
    const mapStateToProps = (state) => ({
      state: state.navigation,
    });

    // Set the navigation component including redux instrumentation
    // We're going to render this as our root component
    this.navigationComponent = connect(mapStateToProps)(App);
    this.setState({ store, rehydrated })
  }

	render() {
    if (this.state.rehydrated) {
      // React doesn't like lowercase component names
      const NavigationAppWrapper = this.navigationComponent

      return(
        <Provider store={this.state.store}>
          <NavigationAppWrapper />
        </Provider>
      )
    } else {
      return null
    }
	}
}
