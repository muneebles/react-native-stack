import { put, all } from 'redux-saga/effects'
import * as sagas from '../sagas'

export default function* rootSaga() {
  const initializedSagas = Object.values(sagas).map((featureSaga) => featureSaga())
  yield all(initializedSagas)
}
