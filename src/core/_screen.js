import React, { Component } from 'react'

export const createScreen = (WrappedComponent) => {
  class ScreenComponent extends Component {
    render() {
      return (
        <WrappedComponent { ...this.props.navigation.state.params }/>
      )
    }
  }

  return ScreenComponent
}
