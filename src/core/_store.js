import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga'
import { offline } from '@redux-offline/redux-offline';
import { createOffline } from '@redux-offline/redux-offline';
import defaultOfflineConfig from '@redux-offline/redux-offline/lib/defaults';
import {
  reduxifyNavigator,
  createReactNavigationReduxMiddleware,
  createNavigationReducer,
} from 'react-navigation-redux-helpers';


import saga from './_saga'
import reducer from './_reducer'


// Create function to initialize a redux store
export function configureStore(DevTools, initialState) {
  let composeEnhancers = compose
  if (__DEV__) {
    // Define compose function with devtools instrumentation
    // Only instrument the store in development mode
    if (typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) {
      composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Set the action history limit for devtools
        maxAge: 500
      })
    }
  }

  return new Promise((resolve, reject) => {
    let store;

    // Create saga middleware instance
    const sagaMiddleware = createSagaMiddleware()

    // Create configuration for redux-offline including the persistCallback
    const offlineConfig = {
      ...defaultOfflineConfig,
      persistCallback: () => {
        // Handle async rehydration
        // We want to hold onto the store until rehydration is complete
        // Store value is returned via this promise, after saga middleware runs
        return resolve(store)
      }
    }

    // Create middleware for redux-offline
    const {
      middleware: offlineMiddleware,
      enhanceReducer,
      enhanceStore
    } = createOffline(offlineConfig);

    // Create middleware for navigation
    // Note: createReactNavigationReduxMiddleware must be run before reduxifyNavigator
    // Currently this is being run in _root.js, the RootComponent::componentWillMount
    const navigationMiddleware = createReactNavigationReduxMiddleware(
      "root",
      state => state.navigation,
    );

    // Compose multiple store enhancers into a single one
    // applyMiddleware converts middleware to a store enhancer
    const enhancer = composeEnhancers(
      applyMiddleware(navigationMiddleware, sagaMiddleware, offlineMiddleware),
      enhanceStore
    )

    // Create store instance
    store = createStore(
      enhanceReducer(reducer),
      enhancer
    );

    // Start saga middleware intercepting actions
    sagaMiddleware.run(saga)
  })
}
