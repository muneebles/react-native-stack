import { connect } from 'react-redux';
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, ScrollView } from 'react-native';
import { NavigationService } from '../services/navigation'


class Demo1Component extends Component {
  constructor(props) {
    super(props)


    // ===============================================
    // EDIT THIS AREA


    // EDIT THIS FIRST
    this.incrementBy = 1


    // EDIT THIS SECOND
    this.useLocalState = true


    // EDIT THIS AREA
    // ===============================================


    this.state = {
      message: 'Initial counter value',
      counter: (this.useLocalState ? 0 : props.counter || 0)
    }
  }

  componentWillUnmount = () => {
    this.stopCounter()
  }

  toggleCounter = () => {
    if(this.state.isCounterRunning) {
      this.stopCounter()
    } else {
      this.startCounter()
    }
  }

  stopCounter = () => {
    clearInterval(this.state.intervalId)
    this.setState({
      intervalId: undefined,
      isCounterRunning: false
    })
  }

  startCounter = () => {
    const createInterval = () => {
      return setInterval(() => {
        if (this.useLocalState) {
          this.localStateUpdate(this.incrementBy)
        } else {
          this.reduxStoreUpdate(this.incrementBy)
        }
      }, 1000)
    }

    this.setState({
      intervalId: createInterval(),
      isCounterRunning: true
    })
  }

  componentWillReceiveProps = (newProps) => {
    const { counter } = newProps
    this.setState({
      counter
    })
  }

  localStateUpdate = (payload) => {
    this.setState({
      message: 'Counter in local state',
      counter: this.state.counter + payload
    })
  }

  reduxStoreUpdate = (payload) => {
    this.setState({
      message: 'Counter in redux store'
    })
    this.props.dispatch({ type: 'INCREMENT', payload })
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Something to edit</Text>
        <Text style={styles.welcome}>{this.state.message}: {this.state.counter}</Text>
        <View style={styles.buttonLayout}>
          <View style={styles.button} >
            <Button
              onPress={this.toggleCounter}
              title={this.state.isCounterRunning ? "Stop Counter" : "Start counter" }/>
          </View>
        </View>
      </View>
    );
  }
}


function mapStateToProps(state) {
  return {
    counter: state.counter.counterValue
  }
}


export const Demo1 = connect(mapStateToProps)(Demo1Component)


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 25,
    textAlign: 'center',
    margin: 10,
    paddingBottom: 30,
  },
  buttonLayout: {
    flexDirection: 'row'
  },
  button: {
    padding: 4,
    height: 45
  }
});
