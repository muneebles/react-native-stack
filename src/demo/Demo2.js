import { connect } from 'react-redux';
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, ScrollView } from 'react-native';
import { createTestRequest } from './actionCreators'


class Demo2Component extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  handleIncrement = () => {
    this.props.dispatch({ type: 'INCREMENT' })
  }

  handleRequest = () => {
    this.props.dispatch(createTestRequest(this.props.counter))
  }

  getText = () => {
    if (this.props.testRequestInProgress) {
      return 'Loading'
    } else {
      return this.props.testResponse
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Offline retries for {this.props.counter}</Text>
        <Text style={styles.welcome}>{this.getText()}</Text>
        <View style={styles.buttonLayout}>
          <View style={styles.button} >
            <Button
              onPress={this.handleIncrement}
              title="Increment counter"/>
          </View>
          <View style={styles.button} >
            <Button
              disabled={this.props.testRequestInProgress}
              onPress={this.handleRequest}
              title="Perform request"/>
          </View>
        </View>
      </View>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    counter: state.counter.counterValue,
    testRequestInProgress: state.demo.inProgress,
    testResponse: state.demo.response,
  }
}


export const Demo2 = connect(mapStateToProps)(Demo2Component)


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 25,
    textAlign: 'center',
    margin: 10,
    paddingBottom: 30,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  buttonScrollContainer: {
    overflow: 'scroll',
  },
  buttonLayout: {
    flexDirection: 'row'
  },
  button: {
    padding: 4,
    height: 45
  }
});
