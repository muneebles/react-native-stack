import { connect } from 'react-redux';
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, ScrollView } from 'react-native';
import { createTestRequest } from './actionCreators'


class Demo3Component extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>That's all I've got</Text>
      </View>
    );
  }
}

export const Demo3 = connect()(Demo3Component)


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 25,
    textAlign: 'center',
    margin: 10,
    paddingBottom: 30,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  buttonScrollContainer: {
    overflow: 'scroll',
  },
  buttonLayout: {
    flexDirection: 'row'
  },
  button: {
    padding: 4,
    height: 45
  }
});
