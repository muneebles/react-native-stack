import { TEST_REQUEST, TEST_REQUEST_SUCCESS, TEST_REQUEST_FAILURE } from './actionTypes'

export const createTestRequest = (testId) => ({
  type: TEST_REQUEST,
  meta: {
    offline: {
      effect: { url: `http://10.0.2.2:5000/test/${testId}` },
        commit: { type: TEST_REQUEST_SUCCESS },
        rollback: { type: TEST_REQUEST_FAILURE }
    }
  }
})
