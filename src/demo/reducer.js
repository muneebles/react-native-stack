import {
  TEST_REQUEST,
  TEST_REQUEST_SUCCESS,
  TEST_REQUEST_FAILURE,
} from './actionTypes'

const getInitialState = () => {
  return {
    inProgress: false,
    completed: false
  }
}

export function demo(state = getInitialState(), action) {
  switch (action.type) {
    case TEST_REQUEST:
      return {
        ...state,
        inProgress: true,
        success: false,
        completed: false
      }
    case TEST_REQUEST_SUCCESS:
      return {
        ...state,
        response: action.payload,
        inProgress: false,
        ...action.meta
      }
    case TEST_REQUEST_FAILURE:
      return {
        ...state,
        inProgress: false,
        ...action.meta
      }
    default:
      return state
  }
}
