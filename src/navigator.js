import {
  createStackNavigator,
  createMaterialTopTabNavigator,
  createAppContainer
} from 'react-navigation'

import { SimpleScreen } from './SimpleScreen'
import { createScreen } from './core/_screen'
import { Demo3 } from './demo/Demo3'
import { Demo1 } from './demo/Demo1'
import { Demo2 } from './demo/Demo2'
import { Home } from './Home'
import {
  RegistrationLanding,
  RegistrationPhoneNumber,
  RegistrationIdNumber,
  RegistrationProfession,
  RegistrationVerify,
  RegistrationComplete
} from './registration/containers'


const HomeStack = createStackNavigator({
  Home: createScreen(Home),
  Details: createScreen(SimpleScreen),
}, {
  headerMode: 'none'
});


const RegistrationStack = createStackNavigator({
  RegistrationLanding: createScreen(RegistrationLanding),
  RegistrationPhoneNumber: createScreen(RegistrationPhoneNumber),
  RegistrationIdNumber: createScreen(RegistrationIdNumber),
  RegistrationProfession: createScreen(RegistrationProfession),
  RegistrationVerify: createScreen(RegistrationVerify),
  RegistrationComplete: createScreen(RegistrationComplete),
}, {
  headerMode: 'none'
});


export const TabNavigator = createMaterialTopTabNavigator({
  'State vs Store': Demo1,
  'Offline retries': Demo2,
  'Donezo': Demo3,
}, {
  tabBarColor: '#FF0000',
  shifting: true,
  swipeEnabled: true
});
