export { registration } from './registration/reducer'
export { demo } from './demo/reducer'

const getInitialState = () => ({
  counterValue: 0
})

export function counter(state = getInitialState(), action) {
  switch (action.type) {
    case 'INCREMENT':
      return {
        ...state,
        counterValue: state.counterValue + (action.payload || 1)
      };
    default:
      return state;
  }
}

