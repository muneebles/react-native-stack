import {
  REGISTRATION_BEGIN,
  REGISTRATION_PROVIDE_PHONE_NUMBER,
  REGISTRATION_PROVIDE_ID_NUMBER,
  REGISTRATION_PROVIDE_PROFESSION,
  REGISTRATION_SUBMIT,
  REGISTRATION_SUBMIT_SUCCESS,
  REGISTRATION_SUBMIT_FAILURE,
  REGISTRATION_END
} from './actionTypes'

export function beginRegistration() {
  return {
     type: REGISTRATION_BEGIN
  }
}

export function providePhoneNumber(phoneNumber) {
  return {
    type: REGISTRATION_PROVIDE_PHONE_NUMBER,
    payload: { phoneNumber }
  }
}

export function provideIdNumber(idNumber) {
  return {
    type: REGISTRATION_PROVIDE_ID_NUMBER,
    payload: { idNumber }
  }
}

export function provideProfession(profession) {
  return {
    type: REGISTRATION_PROVIDE_PROFESSION,
    payload: { profession }
  }
}


export function submitRegistration() {
  return {
    type: REGISTRATION_SUBMIT,
    meta: {
      offline: {
        effect: {
          url: 'http://10.0.2.2:5000/register'
        },
        commit: { type: REGISTRATION_SUBMIT_SUCCESS },
        rollback: { type: REGISTRATION_SUBMIT_FAILURE }
      }
    }
  }
}

export function completeRegistration() {
  return {
     type: REGISTRATION_END
  }
}
