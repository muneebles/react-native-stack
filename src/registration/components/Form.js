import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, ScrollView } from 'react-native';

export class Form extends Component {
  static defaultProps = {
    text: 'I am a form'
  }

  render() {
    return (
      <View><Text>{this.props.text}</Text></View>
    )
  }
}
