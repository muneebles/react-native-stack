import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Platform, StyleSheet, Text, View, Button, ScrollView } from 'react-native';

import { provideIdNumber } from '../actionCreators'


export class RegistrationIdNumberComponent extends Component {
  static defaultProps = {
    text: 'this is a registration id number'
  }

  handleClick = () => {
    this.props.provideIdNumber()
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.layoutContainer} >
          <Text style={styles.titleText}>
            {this.props.text}
          </Text>
        </View>
        <View style={styles.button} >
          <Button
            onPress={this.handleClick}
            title="Continue"/>
        </View>
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    provideIdNumber
  }, dispatch)
}

const styles = StyleSheet.create({
  titleText: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  container: {
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  layoutContainer: {
    paddingTop: 100,
    paddingBottom: 100
  },
  button: {
    width: 300,
    padding: 4,
    height: 245
  }
});


export const RegistrationIdNumber = connect(null, mapDispatchToProps)(RegistrationIdNumberComponent)
