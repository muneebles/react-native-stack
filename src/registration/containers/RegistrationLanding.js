import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Platform, StyleSheet, Text, View, Button, ScrollView } from 'react-native';
import { beginRegistration } from '../actionCreators'

export class RegistrationLandingComponent extends Component {
  static defaultProps = {
    text: 'this is a registration landing view'
  }

  handleClick = () => {
    this.props.beginRegistration()
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.layoutContainer} >
          <Text style={styles.titleText}>
            {this.props.text}
          </Text>
        </View>
        <View style={styles.button} >
          <Button
            onPress={this.handleClick}
            title="Perform request"/>
        </View>
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    beginRegistration
  }, dispatch)
}

const styles = StyleSheet.create({
  titleText: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  container: {
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  layoutContainer: {
    paddingTop: 100,
    paddingBottom: 100
  },
  button: {
    width: 300,
    padding: 4,
    height: 245
  }
});

export const RegistrationLanding = connect(null, mapDispatchToProps)(RegistrationLandingComponent)
