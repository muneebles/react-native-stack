import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Platform, StyleSheet, Text, View, Button, ScrollView } from 'react-native'
import { submitRegistration } from '../actionCreators'


export class RegistrationVerifyComponent extends Component {
  static defaultProps = {
    text: 'this is a registration verify view'
  }

  handleClick = () => {
    this.props.submitRegistration({
      ...this.props.registrationState
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.layoutContainer} >
          <Text style={styles.titleText}>
            {this.props.text}
          </Text>
        </View>
        <View style={styles.button} >
          <Button
            onPress={this.handleClick}
            title="Submit request"/>
        </View>
      </View>
    )
  }
}


const mapStateToProps = (state) => ({
  registrationState: {
    phoneNumber: state.registration.phoneNumber,
    idNumber: state.registration.idNumber,
    profession: state.registration.profession,
  }
})

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    submitRegistration
  }, dispatch)
}

const styles = StyleSheet.create({
  titleText: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  container: {
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  layoutContainer: {
    paddingTop: 100,
    paddingBottom: 100
  },
  button: {
    width: 300,
    padding: 4,
    height: 245
  }
});


export const RegistrationVerify = connect(mapStateToProps, mapDispatchToProps)(RegistrationVerifyComponent)
