import {
  REGISTRATION_BEGIN,
  REGISTRATION_PROVIDE_PHONE_NUMBER,
  REGISTRATION_PROVIDE_ID_NUMBER,
  REGISTRATION_PROVIDE_PROFESSION,
  REGISTRATION_END
} from './actionTypes'

const getInitialState = () => {
  return {
    inProgress: false,
    completed: false
  }
}

export function registration(state = getInitialState(), action) {
  switch (action.type) {
    case REGISTRATION_BEGIN:
      return state
    case REGISTRATION_PROVIDE_PHONE_NUMBER:
      return state
    case REGISTRATION_PROVIDE_ID_NUMBER:
      return state
    case REGISTRATION_PROVIDE_PROFESSION:
      return state
    case REGISTRATION_END:
      return state
    default:
      return state
  }
}
