import { all, put, takeEvery, select } from 'redux-saga/effects'

import { NavigationService } from '../services/navigation'

import {
  REGISTRATION_BEGIN,
  REGISTRATION_PROVIDE_PHONE_NUMBER,
  REGISTRATION_PROVIDE_ID_NUMBER,
  REGISTRATION_PROVIDE_PROFESSION,
  REGISTRATION_SUBMIT_SUCCESS
} from './actionTypes'

import { completeRegistration } from './actionCreators'

function* handleRegistrationBegin(action) {
  NavigationService.navigate('RegistrationPhoneNumber')
}

function* handlePhoneNumber(action) {
  NavigationService.navigate('RegistrationIdNumber')
}

function* handleIdNumber(action) {
  NavigationService.navigate('RegistrationProfession')
}

function* handleProfession(action) {
  NavigationService.navigate('RegistrationVerify')
}

function* handleRegistrationSubmit(action) {
  NavigationService.navigate('RegistrationComplete')
}

function* handleSubmitSuccess(action) {
  NavigationService.navigate('Home')
  yield put(completeRegistration())
}

export function* registration() {
  yield all([
    takeEvery(REGISTRATION_BEGIN, handleRegistrationBegin),
    takeEvery(REGISTRATION_PROVIDE_PHONE_NUMBER, handlePhoneNumber),
    takeEvery(REGISTRATION_PROVIDE_ID_NUMBER, handleIdNumber),
    takeEvery(REGISTRATION_PROVIDE_PROFESSION, handleProfession),
    takeEvery(REGISTRATION_SUBMIT_SUCCESS, handleSubmitSuccess)
  ])
}
