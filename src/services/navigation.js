import { NavigationActions } from 'react-navigation';


class NavigationServiceSingleton {
  constructor() {
    // dispatch to be provided to this service after store initialization
    this.dispatch = null
  }

  setDispatchFunction(dispatchFunction) {
    console.log('Updating top level navigation ref')
    this.dispatch = dispatchFunction
  }

  navigate(routeName, params) {
    console.log("Navigating somewhere")
    const navigateAction = NavigationActions.navigate({
      routeName: routeName,
      params: params,
    });

    if (this.dispatch != null) {
      this.dispatch(navigateAction);
    }
  }
}

export const NavigationService = new NavigationServiceSingleton()
